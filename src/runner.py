import argparse
import os

import numpy as np
from sklearn import metrics

import cobra

parser = argparse.ArgumentParser()
parser.add_argument("dataset", help="the dataset to cluster")
parser.add_argument("output", help="file to which resulting clustering will be stored")
parser.add_argument("-nsi","--num_superinstances", help="the number of super-instances",type=int,default=50)
parser.add_argument("-ls","--labels_specified", help="should be set if last column of dataset contains cluster labels",action="store_true")
parser.add_argument("-i","--interactive", help="if this flag is set, the user will be queried for pairwise relations through the command line",action="store_true")

args = parser.parse_args()


with open(args.dataset) as file:
    X = np.asarray([[float(digit) for digit in line.split(',')] for line in file])

labels = None
if args.labels_specified:
    labels = X[:,-1]
    X = X[:,:-1]

clusterer = cobra.COBRA(args.num_superinstances)

if args.interactive:
    # for an interactive run the labels are not passed as the user will be queried through the commandline
    clusterings, runtimes, ml, cl = clusterer.cluster(X, None, range(X.shape[0]))
else:
    clusterings, runtimes, ml, cl = clusterer.cluster(X, labels, range(X.shape[0]))

if not os.path.exists(args.output):
    os.makedirs(args.output)

if args.output.endswith('/'):
    args.output = args.output[:-1]

with open(args.output + '/clustering','w') as f:
    for label in clusterings[-1]:
        f.write(str(label) + "\n")

with open(args.output + '/ml','w') as f:
    for ml_c in ml:
        f.write(str(ml_c[0]) + "," + str(ml_c[1]) + "\n")

with open(args.output + '/cl','w') as f:
    for cl_c in cl:
        f.write(str(cl_c[0]) + "," + str(cl_c[1]) + "\n")

if labels is not None:
    print "The ARI of the final clustering: " + str(metrics.adjusted_rand_score(clusterings[-1],labels))